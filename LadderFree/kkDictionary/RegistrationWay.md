###### 玩转金刚梯>金刚字典>

### 注册渠道：

- 您可以扫他人散发的[ 邀请二维码 ](/LadderFree/kkDictionary/KKInvitationQRCode.md)在[ 金刚中文网 ](/LadderFree/kkDictionary/KKSiteZh.md)注册
- 您也可以点击他人散发的[ 邀请链接 ](/LadderFree/kkDictionary/KKInvitationLink.md)在[ 金刚中文网 ](/LadderFree/kkDictionary/KKSiteZh.md)注册
- 您还可以直接[ 点击此处 ](https://a2z18.github.io/web/l2_reg)在[ 金刚中文网 ](/LadderFree/kkDictionary/KKSiteZh.md)自助注册



#### 返回到
- [玩转金刚梯](/LadderFree/A.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)


