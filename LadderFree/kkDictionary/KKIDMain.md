###### 玩转金刚>金刚字典>


### 金刚主号
- <strong> 金刚主号 </strong >是[ 金刚副号 ](/LadderFree/kkDictionary/KKIDAuxiliary.md)的相对称谓
- [ 金刚 ](/LadderFree/kkDictionary/A2zitpro.md)把具有以下特征的[ 金刚号 ](/LadderFree/kkDictionary/KKID.md)叫<Strong> 金刚主号 </Strong >：
  - 非c9开头
  - 其上捆绑了[ 大宗流量 ](/LadderFree/kkDictionary/KKDataTrafficBulk.md)包
  - 同一时间您可持有多个这种[ 金刚号 ](https://a2z18.github.io/web/kkid)
  - 连通后供您播放视频
  - 使用频率高

#### 返回到
- [玩转金刚梯](/LadderFree/main.md)
- [金刚字典](/LadderFree/kkDictionary/KKDictionary.md)
