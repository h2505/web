###### 金刚梯>金刚帮助>金刚用户类>
### 金刚用户与金刚网的关系

[ 金刚用户 ](/kkuser.md)与[ 金刚网 ](/kksitecn.md)的关系如下：
- 全部[ 金刚用户 ](/kkuser.md)中只有一部分与[ 金刚网 ](/kksitecn.md)有关，其余与[ 金刚网 ](https://a2z18.github.io/web/kksitecn.md)无关
- 与[ 金刚网 ](/kksitecn.md)有关的是使用[ 金刚1.0 金刚号梯 ](/kkproducts1.0.md)的[ 金刚用户 ](/kkuser.md)
- 与[ 金刚网 ](/kksitecn.md)无关的是使用[ 金刚2.0 金刚号梯 ](/kkproducts2.0.md)的[ 金刚用户 ](/kkuser.md)
- 凡拟使用[ 金刚1.0 金刚号梯 ](/kkproducts1.0.md)的自然人，须首先到[ 金刚网 ](/kksitecn.md)注册，成为在册用户（即获得[ 金刚账户 ](/kkaccount.md)）后才能获得[ 金刚1.0金刚号梯 ](/kkproducts1.0.md)
- 凡拟使用[ 金刚2.0 app梯 ](/kkproducts2.0.md)的自然人，当前不必到[ 金刚网 ](/kksitecn.md)注册，只需下载、安装[ 金刚2.0 app梯 ](/kkproducts2.0.md)即可


#### 推荐阅读
- [金刚梯](/dlb.md)
- [金刚帮助](/list_helpkkvpn.md)
- [金刚1.0金刚号梯](/list_helpkkvpn1.0.md)
- [金刚2.0app梯](/list_helpkkvpn2.0.md)
- [金刚用户类](/list_kkuser.md)
